var http = require('http');
var url = require('url');
var querystring = require('querystring');
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8546"));
var express = require('express');
var app = express();
var server = http.Server(app);
//var mysql = require('mysql');
var keythereum = require("keythereum");
var bodyParser = require('body-parser');
var fs = require('fs');
var time = require('x-date');

app.use(bodyParser.json());


//Created token contract available
var uxcabi = [{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"minBalanceForAccounts","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"minimumBalanceInFinney","type":"uint256"}],"name":"setMinBalance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}];

var unicashContract = web3.eth.contract(uxcabi).at('0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2');

var file_format={
		encoding: "utf8",
		flag: "a+" // flag that specifies that it will append stuff
	    };
app.get("/",function(req,res){
	//var GasPrice=0;
	/*web3.eth.getGasPrice(function(error, result){ 
		GasPrice = Number(result);
	    	//console.log("Gas Price is " + GasPrice + " wei");
		console.log("default Gas Price = " + GasPrice + " wei");
		var block = web3.eth.getBlock("latest");
		console.log("gas Limit = " + block.gasLimit);
		console.log("default Max fee = " + web3.fromWei((block.gasLimit*GasPrice), 'ether')+' FOX');

		GasPrice=web3.toWei(1)/block.gasLimit;
		console.log("Nedded Gas Price = " + GasPrice + " wei");
		console.log("New Max fee = " + web3.fromWei((block.gasLimit*GasPrice), 'ether')+' FOX');
	});*/
	
	res.end("Wrong input params");
});

/*app.get("/getPrivKey/:wallet",async function(req,res){
	var DBcon = await DBConnectInit();
	var wallet = req.params.wallet;
	var datadir = "/home/crypto/FOX_Blockchain";
	var password = await GetPassphrase(wallet);

	var keyObject = keythereum.importFromFile(wallet, datadir);
	var privateKey = keythereum.recover(password, keyObject);
	console.log(privateKey.toString('hex'));
	res.end();
});*/

app.post("/newAccount/", function(req, res) {
	var passphrase=req.body.passphrase;
	web3.personal.newAccount(passphrase, function(err, wallet) {
	if (!err){
		console.log("Create new wallet "+wallet);
	  	res.send(JSON.stringify({
			answer:{
				wallet:wallet,
			},
			type:'success',
			msg:''
		}));
		res.end();
	}else {
		console.log("Error create wallet");
		console.log(err);		
		res.end();
	}
	});
});

app.post("/getBalanceByAddress/", function(req, res) {
	console.log("getBalanceByAddress");
	var addresses=req.body;
	var addr_res={};
	for(var addr in addresses){
		var balance = web3.fromWei(web3.eth.getBalance(addresses[addr])).toString();
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
	console.log(addr_res);
	res.end();
});

app.post("/getBalanceUXCByAddress/", function(req, res) {
	console.log("getBalanceUXCByAddress");
	var addresses=req.body;
	var addr_res={};
	for(var addr in addresses){
		var balance = toDec(unicashContract.balanceOf(addresses[addr]).toString());
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
	console.log(addr_res);
	res.end();
});

/*app.get("/getBalanceByAddress/:address", function(req, res) {
	var addresses=JSON.parse(req.params.address);
	var addr_res={};
	for(var addr in addresses){
		var balance = web3.fromWei(web3.eth.getBalance(addresses[addr])).toString();
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
console.log(addr_res);
//response.send(addr_res);
	res.end();
});*/


app.post("/sendTransaction/", function(req, res) {
	console.log("sendTransaction");
	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6";
	var fee_commission=1;//FOX

	var tx={
		from:from,
		to:to,
		value:web3.toWei(amount,"ether")	
	}
	console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	//console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	
	var tx_fee={
		from:from,
		to:wallet_commission,
		value:web3.toWei(fee_commission,"ether")
	}
	console.log(tx_fee);
	var gas_used_for_fee = web3.eth.estimateGas(tx_fee);
	var fee_2=web3.fromWei(gas_used_for_fee*CurrentGasPrice,"ether");
	//passphrase="42258b65dddbda4309995ac07a32fbe8";
	var total_fee=Number(fee)+Number(fee_2);
	var need_sum_with_fee=Number(amount)+Number(fee_commission)+Number(total_fee);
	console.log("Wallet has:"+web3.eth.getBalance(from)+" Needed:"+web3.toWei(need_sum_with_fee,"ether"));
	console.log("OR Wallet has:"+web3.fromWei(web3.eth.getBalance(from),"ether")+" Needed:"+need_sum_with_fee);
	if(web3.fromWei(web3.eth.getBalance(from),"ether")>need_sum_with_fee){
		web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) {
		  if (!err){
			console.log("Send main transaction");
			console.log("Gas used (fee)= "+gas_used);
			console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
			console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
			console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
			console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));

			console.log("fee:"+fee+" fee_2 "+fee_2);
			console.log("fee sum= "+web3.toWei(Number(fee)+Number(fee_2),"ether"));
			
			  web3.personal.sendTransaction(tx_fee,passphrase, function(err, feeBHash) {
			    if (!err){
				var tx_fee_b={
					from:"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56",
					to:wallet_commission,
					value:web3.toWei(total_fee,"ether")
				}
				web3.personal.sendTransaction(tx_fee_b,"4b2442e95515e0b8ebf4c5cfc8cb36c8", function(err, feeBHash) {
				    if (!err){
						console.log(transactionHash);
						res.send(JSON.stringify({
							answer:{
								txhash:transactionHash,
								fee_blockchain:total_fee,
								fee:1,
								fee_blockchain_hash:feeBHash
							},
							type:'success',
							msg:''
						}));
						res.end();
				    }else {console.log(err);res.end();}
				  });
			    }else {console.log(err);res.end();}
			  });
	  	  }else {console.log(err);res.end();}
		  
		});
	}else{
		console.log('Not enough balance');		
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
});


app.get("/listAccounts/",function(req,res){
//console.log(web3.eth.accounts);// in web3.eth.accounts
	//for(i=0;i<90;i++){
	for(i in web3.eth.accounts){
		var account = web3.eth.accounts[i];//"\tbalance:" + omisegoContract.balanceOf(account) + " UXC" + " and "+
		res.write("Account["+i+"]:\t"+ account + "\tbalance:"+ web3.fromWei(web3.eth.getBalance(account),'ether')+" FOX \tand "+toDec(unicashContract.balanceOf(account)).toFixed(8)+" UXC" + "\n");
		//console.log(unicashContract.balanceOf(account));		
		//if(i>5) return;
	}
	//var acc=['0x1fa7baba2df63499e05c7ab00b43df0aa8709f3f','0xd3728023da5cfe4a7cbf54d672ace80f427d91b5','0x362fef7aa46778647c143cada6a3d186e2266a03','0xcc009d311365afa9b3b4c2a2ce5c22e6d9248ff4','0x672c0f498e59426f67b8cd932a63c37c706f8fa0','0x035333e87cc1925329e48487bbd0d330737eda94'];
	//for(ii in acc){
	//	var account = acc[ii];
		//console.log("Account["+ii+"]: "+ account+ " "+web3.eth.getBalance(account));
		//res.write("Account["+ii+"]: "+ account + "\tbalance:"+ web3.fromWei(web3.eth.getBalance(account),'ether')+" FOX"+"\n");
	//}
	//web3.eth.accounts.forEach(function(e,i){
	//	res.write("eth.accounts["+i+"]: "+ web3.eth.accounts[i] + "\tbalance:" + omisegoContract.balanceOf(web3.eth.accounts[i]) + " UXC" + " and "+ web3.eth.getBalance(web3.eth.accounts[i])+" FOX"+"\n")
	//});
	res.end();
});

//request for cron
app.post("/getBalanceAddresses/",function(req,res){
	console.log("getBalanceAddresses");
	var wallets={};
	var wallets_arr=req.body.wallets;
	//console.log(wallets_arr);
	if(wallets_arr){
		for(i in wallets_arr){
			var account = wallets_arr[i];
			wallets[account]={
				'fox':{balance:web3.fromWei(web3.eth.getBalance(account),'ether')},
				'uxc':{balance:toDec(unicashContract.balanceOf(account))}
			};
		}
		res.send(JSON.stringify({
			answer:{
				wallets:wallets,
			},
			type:'success',
			msg:''
		}));
	}
	res.end();
});

app.post("/getCnrBalanceAddresses/",function(req,res){
	console.log("getCnrBalanceAddresses");
	var wallets={};
	var wallets_arr=req.body.wallets;
	//console.log(wallets_arr);
	if(wallets_arr){
		for(i in wallets_arr){
			var account = wallets_arr[i];
			wallets[account]={
				'fox':{balance:web3.fromWei(web3.eth.getBalance(account),'ether')},
				'cnr':{balance:Number(toDec(unicashContract.balanceOf(account)))}
			};
		}
		res.send(JSON.stringify({
			answer:{
				wallets:wallets,
			},
			type:'success',
			msg:''
		}));
	}
	res.end();
});

/*app.get("/getBalanceAddresses/",function(req,res){
	var wallets={};
	var wallets_arr=['0x796d2ecd0348d940e387d899d251ec91bf35d24a','0x38d01653e0ce8375a1f7dd3110b1a38d87b8749b'];
	console.log(wallets_arr);
	if(wallets_arr){
		for(i in wallets_arr){
			var account = wallets_arr[i];
			wallets[account]={
				'eth':{balance:web3.fromWei(web3.eth.getBalance(account),'ether')},
				'cnr':{balance:toDec(unicashContract.balanceOf(account))}
			};
		}
		res.send(JSON.stringify({
			answer:{
				wallets:wallets,
			},
			type:'success',
			msg:''
		}));
	}
	res.end();
});*/



app.post("/sendCnrTransaction/", function(req, res) {
	console.log("sendCnrTransaction");
	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;

	let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	console.log("wallet from:"+from+" Balance:"+Number(web3.eth.getBalance(from))+"ETH and "+unicashContract.balanceOf(from)+" CNR");
	console.log("Need: "+fromDec(amount)+" CNR and "+fee+"ETH");
	if(unicashContract.balanceOf(from)>fromDec(amount)){
		if(Number(web3.eth.getBalance(from))>web3.toWei(Number(fee),"ether")){
			web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //first transaction
			if (!err){
				console.log("Send transaction");
				console.log("Gas used (fee)= "+gas_used);
				console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
				console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
				console.log("fee:"+fee);
				console.log("fee sum= "+web3.toWei(Number(fee),"ether"));
				console.log("Hash:"+transactionHash);
			  	res.send(JSON.stringify({
					answer:{
						txhash:transactionHash,
						fee_blockchain:fee
					},
					type:'success',
					msg:''
				}));
				res.end();
		  	  }else {console.log(err);res.end();}
			});
		}else{
			console.log(web3.eth.getBalance(from)+">"+web3.toWei(Number(fee),"ether")+" result:"+(web3.eth.getBalance(from)>web3.toWei(Number(fee),"ether")));
			res.send(JSON.stringify({
				answer:null,
				type:'error',
				msg:'Not enough balance'
			}));
			res.end();
		}
	}else{
console.log(unicashContract.balanceOf(from)+">"+fromDec(amount)+" result:"+(unicashContract.balanceOf(from)>fromDec(amount)));
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
});


/*app.get("/sendCnrTransaction1/", function(req, res) {
	
	var from="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc";
	var to="0x796d2ecd0348d940e387d899d251ec91bf35d24a";
	var amount=50000;
	var passphrase="12345";
	var CurrentGasPrice=web3.eth.gasPrice;

	let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";
	var wallet_commission="";
	var fee_commission=1;//UXC

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	console.log(tx);

	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	
	//let transfer_data_fee = unicashContract.transfer.getData(wallet_commission,fromDec(fee_commission),{from: from});		

	
	//var gas_used_for_fee = web3.eth.estimateGas(tx_fee);

	//var fee_2=web3.fromWei(gas_used_for_fee*CurrentGasPrice,"ether");
	//var total_fee=Number(fee)+Number(fee_2);
	if(unicashContract.balanceOf(from)>fromDec(amount)){
		//if(web3.eth.getBalance(from)>web3.toWei(total_fee,"ether")){
			//passphrase="42258b65dddbda4309995ac07a32fbe8";
			web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //first transaction
			if (!err){
				console.log("Send main transaction");
				console.log("Gas used (fee)= "+gas_used);
				console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
				console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
				//console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
				//console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));
				  
				//console.log("fee:"+fee+" fee_2 "+fee_2);
				//console.log("fee sum= "+web3.toWei(Number(fee)+Number(fee_2),"ether"));
		
				 res.send(JSON.stringify({
					answer:transactionHash,
					type:'success',
					msg:'send transaction ok'
				}));
				res.end(); 
		  	  }else {console.log(err);res.end();}
			  
			});
		
	}else{
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
});*/

app.post("/genNewCoins/", function(req, res) {
	console.log("genNewCoins");
	var owner="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc";//main token address
	var address=req.body.address;
	var amount=req.body.amount;
	var passphrase="12345";
	var CurrentGasPrice=web3.eth.gasPrice;
	
	let mintToken_data = unicashContract.mintToken.getData(address,fromDec(amount));
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";
	
	var tx={
	  	from: owner,			
		to: wallet_to_contract,
		value:0,
		data:mintToken_data	
	}
	console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	
	web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) {
	if (!err){
		console.log("Send transaction MintToken");
		console.log("Gas used (fee)= "+gas_used);
		console.log("fee:"+fee);
		console.log("fee sum= "+web3.toWei(Number(fee),"ether"));
		
	  	res.send(JSON.stringify({
			answer:{
				txhash:transactionHash,
				fee_blockchain:fee
			},
			type:'success',
			msg:''
		}));
		res.end();
  	  }else {console.log(err);res.end();}
	});
});

app.get("/genNewCoins/", function(req, res) {

	var owner="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc";//main token address
	//var address="0x34ac8b2f52dbaa3d8d082123faf6a3c592da7d8f";
	//var address="0x6d998982ea47a9c29a33e8005aa08a265db67059";
	var amount=1000;
	var passphrase="12345";
	var CurrentGasPrice=web3.eth.gasPrice;
	
	//personal.unlockAccount(address,"12345",2000);
	let mintToken_data = unicashContract.mintToken.getData(address,fromDec(amount));
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";

	var tx={
	  	from: owner,			
		to: wallet_to_contract,
		value:0,
		data:mintToken_data	
	}
	console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	
	web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) {
	if (!err){
		console.log("Send transaction MintToken");
		console.log("Gas used (fee)= "+gas_used);
		console.log("fee:"+fee);
		console.log("fee sum= "+web3.toWei(Number(fee),"ether"));
		
	  	res.send(JSON.stringify({
			answer:{
				txhash:transactionHash,
				fee_blockchain:fee
			},
			type:'success',
			msg:''
		}));
		res.end();
  	  }else {console.log(err);res.end();}
	});
});



app.get("/sut/", function(req, res) {
	
	var time_start = new Date().format('yyyy-mm-dd HH:MM:ss');
	save_log("transaction","\n\nStart transaction at "+ time_start+"\n");
	var from="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc";
	var to="0xd64802032cf58857ed15c222f10a3e01b3f8420a";
	var amount=1;
	var passphrase="12345";
	var CurrentGasPrice=web3.eth.gasPrice;
	
	let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2"; //test token address
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6"; //FOX and UXC //Fee
	var wallet_fox_exchange="0xd3728023da5cfe4a7cbf54d672ace80f427d91b5"; //ICO
	var pass_wallet_fox_exchange="88daa4765a3de2a21d7e8609c450acd9"; //ICO pass
	//var coinbase="0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"; //Miner dev
	//var pass_cb="4b2442e95515e0b8ebf4c5cfc8cb36c8"; //dev
	var coinbase="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc"; //Miner test
	var pass_cb="12345"; //test
	var fee_commission_uxc=0.1;//UXC
	var fee_commission_fox=0.5;//FOX

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	//console.log(tx);
	save_log("transaction","Data:\n Wallet from:"+ from +"\nWallet to:"+ to +"\nAmount:"+ amount +" ("+fromDec(amount)+")\n");
	save_log("transaction",JSON.stringify(tx));
	
	let gas_used = web3.eth.estimateGas(tx);
	
	//console.log("Gas Price: "+CurrentGasPrice);
	
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	save_log("transaction","Fee:"+ fee +"\n");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	save_log("transaction","Gas Price:"+ CurrentGasPrice +"\nFee:"+ fee +"\n");
	let commission_exchange_uxc = unicashContract.transfer.getData(wallet_commission,fromDec(fee_commission_uxc),{from: from});		

	var tx_commission_exchange_uxc={
		to: wallet_to_contract,
		from: from,
		value: 0,
		data:commission_exchange_uxc
	}
	var gas_used_for_fee_uxc = web3.eth.estimateGas(tx_commission_exchange_uxc);
	var fee_2=web3.fromWei(gas_used_for_fee_uxc*CurrentGasPrice,"ether");
	save_log("transaction","Transaction UXC fee:\n");
	save_log("transaction",JSON.stringify(tx_commission_exchange_uxc));
	save_log("transaction","Fee commission:"+ fee_2 +"\n");
	var total_fee=Number(fee)+Number(fee_2);
	
	var tx_fox_exchange={
		to: wallet_commission,
		from: wallet_fox_exchange,
		value: web3.toWei(Number(fee_commission_fox),"ether")
	}
	var gas_used_for_tx_fox_exchange = web3.eth.estimateGas(tx_fox_exchange);
	var fee_3=web3.fromWei(gas_used_for_tx_fox_exchange*CurrentGasPrice,"ether");
	
	save_log("transaction","Transaction FOX exchange to commission:\n");
	save_log("transaction",JSON.stringify(tx_fox_exchange));
	save_log("transaction","\nFee FOX exchange to commission:"+ fee_3 +"\n");
	
	var tx_fox_exchange_to={
		to: to,
		from: wallet_fox_exchange,
		value: web3.toWei(Number(fee_commission_fox),"ether")
	}
	var gas_used_for_tx_fox_exchange_to = web3.eth.estimateGas(tx_fox_exchange_to);
	var fee_4=web3.fromWei(gas_used_for_tx_fox_exchange_to*CurrentGasPrice,"ether");
	
	save_log("transaction","Transaction FOX exchange to receiver:\n");
	save_log("transaction",JSON.stringify(tx_fox_exchange_to));
	save_log("transaction","\nFee FOX exchange to receiver:"+ fee_4 +"\n");
	
	var another_total_fee=Number(fee_3)+Number(fee_4);
	var sum_of_all_fee=Number(total_fee)+Number(another_total_fee);
	var tx_fee_return={
		from: coinbase,
		to: wallet_commission,
		value: Math.round(web3.toWei(sum_of_all_fee,"ether"))
	}
	save_log("transaction","Transaction FOX fee from coinbase to wallet commission:\n");
	save_log("transaction",JSON.stringify(tx_fee_return));
	
	save_log("transaction","\nAnother total fee: "+another_total_fee);
	save_log("transaction","\nSum of all fee: "+sum_of_all_fee);
	
	save_log("transaction","\nStart SEND Transaction");
	if(unicashContract.balanceOf(from)>fromDec(amount+fee_commission_uxc)){
		if(web3.eth.getBalance(from)>web3.toWei(total_fee,"ether")){
			save_log("transaction","\nwallet_fox_exchange Balance: "+Number(web3.eth.getBalance(wallet_fox_exchange)));
			save_log("transaction","\nAnother_total_fee: "+web3.toWei(another_total_fee,"ether")+"\n");
			if(Number(web3.eth.getBalance(wallet_fox_exchange))>web3.toWei(another_total_fee,"ether")){
				web3.personal.sendTransaction(tx_commission_exchange_uxc,passphrase, function(err, feeBHash1) { // transaction commission UXC
					if (!err){
						console.log("Send transaction commission UXC: ok\nHash: "+feeBHash1+"\n");
						save_log("transaction","Send transaction commission UXC: ok\nHash: "+feeBHash1+"\n");
						web3.personal.sendTransaction(tx_fox_exchange,pass_wallet_fox_exchange, function(err, feeBHash3) { // transaction exchange FOX to wallet commission
							if (!err){
								console.log("Send transaction exchange FOX to wallet commission: ok\nHash: "+feeBHash3+"\n");
								save_log("transaction","Send transaction exchange FOX to wallet commission: ok\nHash: "+feeBHash3+"\n");
								web3.personal.sendTransaction(tx_fox_exchange_to,pass_wallet_fox_exchange, function(err, feeBHash4) { // transaction exchange FOX to receiver
									if (!err){
										console.log("Send transaction exchange FOX to receiver: ok\nHash: "+feeBHash4+"\n");
										save_log("transaction","Send transaction exchange FOX to receiver: ok\nHash: "+feeBHash4+"\n");
										web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //main transaction
											if (!err){
												console.log("Send main transaction: ok\nHash: "+transactionHash+"\n");
												save_log("transaction","Send main transaction: ok\nHash: "+transactionHash+"\n");
												web3.personal.sendTransaction(tx_fee_return,pass_cb, function(err, feeBHash2) { // transaction send sum of all fee to wallet commission
													if (!err){
														console.log("Send sum of all fee to wallet commission: ok\nHash: "+feeBHash2+"\n");
														save_log("transaction","Send sum of all fee to wallet commission: ok\nHash: "+feeBHash2+"\n");
														//console.log(transactionHash);
														//console.log("Send main transaction");
														//console.log("passphrase= "+passphrase);
														//console.log("Gas used (fee)= "+gas_used);
														//console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
														//console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
														//console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
														//console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));
														  
														//console.log("fee:"+fee+" fee_2 "+fee_2);
														//console.log("fee sum= "+Math.round(web3.toWei(total_fee,"ether")));
														res.send(JSON.stringify({
															answer:{
																txhash:transactionHash,
																fee_blockchain:total_fee,
																fee:fee_commission_uxc,
																fee_blockchain_hash:feeBHash1
															},
															type:'success',
															msg:''
														}));
														res.end();
													}else {console.log(err);res.end();}
												});
											}else {console.log(err);res.end();}
										});
									}else {console.log(err);res.end();}
								});
							}else {console.log(err);res.end();}
						});
					}else {console.log(err);res.end();}
				});
			}else{
				res.send(JSON.stringify({
					answer:{code:437}, //not enough FOX on wallet fox exchange
					type:'error',
					msg:'Not enough balance'
				}));
				res.end();
			}
		}else{
			res.send(JSON.stringify({
				answer:{code:435}, //not enough FOX
				type:'error',
				msg:'Not enough balance'
			}));
			res.end();
		}
	}else{
		res.send(JSON.stringify({
			answer:{code:436}, //not enough UXC
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
	//res.end();
});

app.post("/sendUxcTransaction/", function(req, res) {
	var time_start = new Date().format('yyyy-mm-dd HH:MM:ss');
	save_log("transaction","\n\nStart transaction at "+ time_start+"\n");
	console.log("sendUxcTransaction");
	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;

	let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6";
	//var fee_commission=1;//UXC



	
	//var from="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc";
	//var to="0xd64802032cf58857ed15c222f10a3e01b3f8420a";
	//var amount=1;
	//var passphrase="12345";
	//var CurrentGasPrice=web3.eth.gasPrice;
	
	//let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	//var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2"; //test token address
	//var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6"; //FOX and UXC //Fee
	var wallet_fox_exchange="0xd3728023da5cfe4a7cbf54d672ace80f427d91b5"; //ICO
	var pass_wallet_fox_exchange="88daa4765a3de2a21d7e8609c450acd9"; //ICO pass
	//var coinbase="0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"; //Miner dev
	//var pass_cb="4b2442e95515e0b8ebf4c5cfc8cb36c8"; //dev
	var coinbase="0xa56da9c88a61f24efa041a4bc71a829caf3f94bc"; //Miner test
	var pass_cb="12345"; //test
	var fee_commission_uxc=0.1;//UXC
	var fee_commission_fox=0.5;//FOX

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	//console.log(tx);
	save_log("transaction","Data:\nWallet from:"+ from +"\nWallet to:"+ to +"\nAmount:"+ amount +" ("+fromDec(amount)+")\n");
	save_log("transaction",JSON.stringify(tx));
	
	let gas_used = web3.eth.estimateGas(tx);
	
	//console.log("Gas Price: "+CurrentGasPrice);
	
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//save_log("transaction","\nFee:"+ fee +"\n");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	save_log("transaction","\nGas Price:"+ CurrentGasPrice +"\nFee:"+ fee +"\n");
	let commission_exchange_uxc = unicashContract.transfer.getData(wallet_commission,fromDec(fee_commission_uxc),{from: from});		

	var tx_commission_exchange_uxc={
		to: wallet_to_contract,
		from: from,
		value: 0,
		data:commission_exchange_uxc
	}
	var gas_used_for_fee_uxc = web3.eth.estimateGas(tx_commission_exchange_uxc);
	var fee_2=web3.fromWei(gas_used_for_fee_uxc*CurrentGasPrice,"ether");
	save_log("transaction","Transaction UXC fee:\n");
	save_log("transaction",JSON.stringify(tx_commission_exchange_uxc));
	save_log("transaction","\nFee commission:"+ fee_2 +"\n");
	var total_fee=Number(fee)+Number(fee_2);
	
	var tx_fox_exchange={
		to: wallet_commission,
		from: wallet_fox_exchange,
		value: web3.toWei(Number(fee_commission_fox),"ether")
	}
	var gas_used_for_tx_fox_exchange = web3.eth.estimateGas(tx_fox_exchange);
	var fee_3=web3.fromWei(gas_used_for_tx_fox_exchange*CurrentGasPrice,"ether");
	
	save_log("transaction","Transaction FOX exchange to commission:\n");
	save_log("transaction",JSON.stringify(tx_fox_exchange));
	save_log("transaction","\nFee FOX exchange to commission:"+ fee_3 +"\n");
	
	var tx_fox_exchange_to={
		to: to,
		from: wallet_fox_exchange,
		value: web3.toWei(Number(fee_commission_fox),"ether")
	}
	var gas_used_for_tx_fox_exchange_to = web3.eth.estimateGas(tx_fox_exchange_to);
	var fee_4=web3.fromWei(gas_used_for_tx_fox_exchange_to*CurrentGasPrice,"ether");
	
	save_log("transaction","Transaction FOX exchange to receiver:\n");
	save_log("transaction",JSON.stringify(tx_fox_exchange_to));
	save_log("transaction","\nFee FOX exchange to receiver:"+ fee_4 +"\n");
	
	var another_total_fee=Number(fee_3)+Number(fee_4);
	var sum_of_all_fee=Number(total_fee)+Number(another_total_fee);
	var tx_fee_return={
		from: coinbase,
		to: wallet_commission,
		value: Math.round(web3.toWei(sum_of_all_fee,"ether"))
	}
	save_log("transaction","Transaction FOX fee from coinbase to wallet commission:\n");
	save_log("transaction",JSON.stringify(tx_fee_return));
	
	save_log("transaction","\nAnother total fee: "+another_total_fee);
	save_log("transaction","\nSum of all fee: "+sum_of_all_fee);
	
	save_log("transaction","\nStart SEND Transaction");
	if(passphrase!=''){
		if(unicashContract.balanceOf(from)>fromDec(amount+fee_commission_uxc)){
				save_log("transaction","\nfrom Balance FOX: "+Number(web3.eth.getBalance(from)));
				save_log("transaction","\nTotal_fee: "+web3.toWei(total_fee,"ether")+"\n");
			if(Number(web3.eth.getBalance(from))>web3.toWei(total_fee,"ether")){
				save_log("transaction","\nwallet_fox_exchange Balance: "+Number(web3.eth.getBalance(wallet_fox_exchange)));
				save_log("transaction","\nAnother_total_fee: "+web3.toWei(another_total_fee,"ether")+"\n");
				//console.log("compare:"+(Number(web3.eth.getBalance(wallet_fox_exchange))>web3.toWei(another_total_fee,"ether"))+"\n");
				if(Number(web3.eth.getBalance(wallet_fox_exchange))>web3.toWei(another_total_fee,"ether")){
					//console.log("compare:"+(Number(web3.eth.getBalance(wallet_fox_exchange))>web3.toWei(another_total_fee,"ether"))+"\n");
					web3.personal.sendTransaction(tx_commission_exchange_uxc,passphrase, function(err, feeBHash1) { // transaction commission UXC
						//console.log("err:"+"\n");
						//console.log(err);
						//console.log("err end\n");
						if (!err){
							console.log("Send transaction commission UXC: ok\nHash: "+feeBHash1+"\n");
							save_log("transaction","Send transaction commission UXC: ok\nHash: "+feeBHash1+"\n");
							web3.personal.sendTransaction(tx_fox_exchange,pass_wallet_fox_exchange, function(err, feeBHash3) { // transaction exchange FOX to wallet commission
								if (!err){
									console.log("Send transaction exchange FOX to wallet commission: ok\nHash: "+feeBHash3+"\n");
									save_log("transaction","Send transaction exchange FOX to wallet commission: ok\nHash: "+feeBHash3+"\n");
									web3.personal.sendTransaction(tx_fox_exchange_to,pass_wallet_fox_exchange, function(err, feeBHash4) { // transaction exchange FOX to receiver
										if (!err){
											console.log("Send transaction exchange FOX to receiver: ok\nHash: "+feeBHash4+"\n");
											save_log("transaction","Send transaction exchange FOX to receiver: ok\nHash: "+feeBHash4+"\n");
											web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //main transaction
												if (!err){
													console.log("Send main transaction: ok\nHash: "+transactionHash+"\n");
													save_log("transaction","Send main transaction: ok\nHash: "+transactionHash+"\n");
													web3.personal.sendTransaction(tx_fee_return,pass_cb, function(err, feeBHash2) { // transaction send sum of all fee to wallet commission
														if (!err){
															console.log("Send sum of all fee to wallet commission: ok\nHash: "+feeBHash2+"\n");
															save_log("transaction","Send sum of all fee to wallet commission: ok\nHash: "+feeBHash2+"\n");
															//console.log(transactionHash);
															//console.log("Send main transaction");
															//console.log("passphrase= "+passphrase);
															//console.log("Gas used (fee)= "+gas_used);
															//console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
															//console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
															//console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
															//console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));
															  
															//console.log("fee:"+fee+" fee_2 "+fee_2);
															//console.log("fee sum= "+Math.round(web3.toWei(total_fee,"ether")));
															res.send(JSON.stringify({
																answer:{
																	txhash:transactionHash,
																	fee_blockchain:total_fee,
																	fee:fee_commission_uxc,
																	fee_blockchain_hash:feeBHash1
																},
																type:'success',
																msg:''
															}));
															res.end();
														}else {console.log(err);res.end();}
													});
												}else {console.log(err);res.end();}
											});
										}else {console.log(err);res.end();}
									});
								}else {console.log(err);res.end();}
							});
						}else {console.log(err);res.end();}
					});
				}else{
					res.send(JSON.stringify({
						answer:{code:437}, //not enough FOX on wallet fox exchange
						type:'error',
						msg:'Not enough balance'
					}));
					res.end();
				}
			}else{
				res.send(JSON.stringify({
					answer:{code:435}, //not enough FOX
					type:'error',
					msg:'Not enough balance'
				}));
				res.end();
			}
		}else{
			res.send(JSON.stringify({
				answer:{code:436}, //not enough UXC
				type:'error',
				msg:'Not enough balance'
			}));
			res.end();
		}
	}else{
		res.send(JSON.stringify({
			answer:null, //pass empty
			type:'error',
			msg:'empty pass'
		}));
		res.end();
	}
});

app.post("/getLastBlock/", function(req, res) {
	var last_block=web3.eth.blockNumber;
	res.send(JSON.stringify({
		answer:{block:last_block},
		type:'success',
		msg:''
	}));
	res.end();
});

app.post("/getTransactionInfo/", function(req, res) {
	var txhash=req.body.txhash;
	if(txhash!=""){
		var transaction=web3.eth.getTransaction(txhash);
		if(transaction){
			res.send(JSON.stringify({
				answer:transaction,
				type:'success',
				msg:''
			}));
		}else{
			res.send(JSON.stringify({
				answer:null,
				type:'error',
				msg:''
			}));
		}
	}else{
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'empty hash'
		}));
	}
	//console.log(transaction);
	
	res.end();
});

/*app.post("/sendUxcTransaction/", function(req, res) {
	console.log("sendUxcTransaction");
	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;

	let transfer_data = unicashContract.transfer.getData(to,fromDec(amount),{from: from});
	var wallet_to_contract="0x2b81ecc98c65698137b9a7fbacf12fa77fb691d2";
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6";
	var fee_commission=1;//UXC

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	//console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	
	let transfer_data_fee = unicashContract.transfer.getData(wallet_commission,fromDec(fee_commission),{from: from});		

	var tx_fee={
		to: wallet_to_contract,
		from: from,
		value: 0,
		data:transfer_data_fee
	}
	var gas_used_for_fee = web3.eth.estimateGas(tx_fee);

	var fee_2=web3.fromWei(gas_used_for_fee*CurrentGasPrice,"ether");
	var total_fee=Number(fee)+Number(fee_2);
	if(unicashContract.balanceOf(from)>fromDec(amount+fee_commission)){
		if(web3.eth.getBalance(from)>web3.toWei(total_fee,"ether")){
			//passphrase="42258b65dddbda4309995ac07a32fbe8";
			web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //first transaction
			if (!err){
				console.log("Send main transaction");
console.log("passphrase= "+passphrase);
				console.log("Gas used (fee)= "+gas_used);
				console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
				console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
				console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
				console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));
				  
				console.log("fee:"+fee+" fee_2 "+fee_2);
				console.log("fee sum= "+Math.round(web3.toWei(total_fee,"ether")));
		
				  web3.personal.sendTransaction(tx_fee,passphrase, function(err, feeBHash) { //second transaction commission
				    if (!err){
					var tx_fee_b={
						from:"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56",
						to:wallet_commission,
						value:Math.round(web3.toWei(total_fee,"ether"))
					}
					web3.personal.sendTransaction(tx_fee_b,"4b2442e95515e0b8ebf4c5cfc8cb36c8", function(err, feeBHash) { // third transaction send sum fee to wallet fee
					    if (!err){
						console.log(transactionHash);
					  	res.send(JSON.stringify({
							answer:{
								txhash:transactionHash,
								fee_blockchain:total_fee,
								fee:fee_commission,
								fee_blockchain_hash:feeBHash
							},
							type:'success',
							msg:''
						}));
						res.end();
					    }else {console.log(err);res.end();}
					  });
				    }else {console.log(err);res.end();}
				  });
		  	  }else {console.log(err);res.end();}
			  
			});
		}else{
			res.send(JSON.stringify({
				answer:{code:435}, //not enough FOX
				type:'error',
				msg:'Not enough balance'
			}));
			res.end();
		}
	}else{
		res.send(JSON.stringify({
			answer:{code:436}, //not enough UXC
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
//res.end();
});*/

function fromDec(number){
	if(number>=0){
		return number*100000000;	
	}else return false;

}
function toDec(number){
	if(number>=0){
		return number/100000000;	
	}else return false;

}

function save_log(file_type,text){
	name_file="logs/all.txt";
	if(file_type=="transaction") name_file="logs/Uxc_transaction.txt";
	fs.writeFile(
	    name_file,
	    text,
	    file_format,
	    (error) => { }
	);
}

//http.createServer(app).listen(7000);
server.listen(7100);
