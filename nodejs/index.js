var http = require('http');
var url = require('url');
var querystring = require('querystring');
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
var express = require('express');
var app = express();
//var mysql = require('mysql');
var keythereum = require("keythereum");
var bodyParser = require('body-parser');

app.use(bodyParser.json());


//Created token contract available
var uxcabi = [{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"minBalanceForAccounts","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"minimumBalanceInFinney","type":"uint256"}],"name":"setMinBalance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}];

var unicashContract = web3.eth.contract(uxcabi).at('0xc256c37b1d3d9dc25ce0b75c119265a21f54d407');

app.get("/",function(req,res){
	//var GasPrice=0;
	/*web3.eth.getGasPrice(function(error, result){ 
		GasPrice = Number(result);
	    	//console.log("Gas Price is " + GasPrice + " wei");
		console.log("default Gas Price = " + GasPrice + " wei");
		var block = web3.eth.getBlock("latest");
		console.log("gas Limit = " + block.gasLimit);
		console.log("default Max fee = " + web3.fromWei((block.gasLimit*GasPrice), 'ether')+' FOX');

		GasPrice=web3.toWei(1)/block.gasLimit;
		console.log("Nedded Gas Price = " + GasPrice + " wei");
		console.log("New Max fee = " + web3.fromWei((block.gasLimit*GasPrice), 'ether')+' FOX');
	});*/
	
	res.end("Wrong input params");
});

/*app.get("/getPrivKey/:wallet",async function(req,res){
	var DBcon = await DBConnectInit();
	var wallet = req.params.wallet;
	var datadir = "/home/crypto/FOX_Blockchain";
	var password = await GetPassphrase(wallet);

	var keyObject = keythereum.importFromFile(wallet, datadir);
	var privateKey = keythereum.recover(password, keyObject);
	console.log(privateKey.toString('hex'));
	res.end();
});*/

app.post("/newAccount/", function(req, res) {
	var passphrase=req.body.passphrase;
	web3.personal.newAccount(passphrase, function(err, wallet) {
          if (!err){
		console.log("Create new wallet "+wallet);
	  	res.send(JSON.stringify({
			answer:{
				wallet:wallet,
			},
			type:'success',
			msg:''
		}));
		res.end();
  	  }else {
		console.log("Error create wallet");
		console.log(err);		
		res.end();
		}
        });

	
});

app.post("/getBalanceByAddress/", function(req, res) {
	var addresses=req.body;
	var addr_res={};
	for(var addr in addresses){
		var balance = web3.fromWei(web3.eth.getBalance(addresses[addr])).toString();
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
	console.log(addr_res);
	res.end();
});

app.post("/getBalanceUXCByAddress/", function(req, res) {
	var addresses=req.body;
	var addr_res={};
	for(var addr in addresses){
		var balance = toDec(unicashContract.balanceOf(addresses[addr]).toString());
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
	console.log(addr_res);
	res.end();
});

/*app.get("/getBalanceByAddress/:address", function(req, res) {
	var addresses=JSON.parse(req.params.address);
	var addr_res={};
	for(var addr in addresses){
		var balance = web3.fromWei(web3.eth.getBalance(addresses[addr])).toString();
		addr_res[addresses[addr]]={balance};
	}
	res.send(JSON.stringify({
		answer:{
			addr_res
		},
		type:'success',
		msg:''
	}));
console.log(addr_res);
//response.send(addr_res);
	res.end();
});*/


app.post("/sendTransaction/", function(req, res) {

	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6";
	var fee_commission=1;//FOX

	var tx={
		from:from,
		to:to,
		value:web3.toWei(amount,"ether")	
	}
	console.log(tx);
	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	//console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	
	var tx_fee={
		from:from,
		to:wallet_commission,
		value:web3.toWei(fee_commission,"ether")
	}
	console.log(tx_fee);
	var gas_used_for_fee = web3.eth.estimateGas(tx_fee);
	var fee_2=web3.fromWei(gas_used_for_fee*CurrentGasPrice,"ether");
	var total_fee=Number(fee)+Number(fee_2);
	var need_sum_with_fee=Number(amount)+Number(fee_commission)+Number(total_fee);
	console.log("Wallet has:"+web3.eth.getBalance(from)+" Needed:"+web3.toWei(need_sum_with_fee,"ether"));
	console.log("OR Wallet has:"+web3.fromWei(web3.eth.getBalance(from),"ether")+" Needed:"+need_sum_with_fee);
	if(web3.fromWei(web3.eth.getBalance(from),"ether")>need_sum_with_fee){
		web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) {
		  if (!err){
			console.log("Send main transaction");
			console.log("Gas used (fee)= "+gas_used);
			console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
			console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
			console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
			console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));

			console.log("fee:"+fee+" fee_2 "+fee_2);
			console.log("fee sum= "+web3.toWei(Number(fee)+Number(fee_2),"ether"));
			console.log("Transaction hash: "+transactionHash);
			  web3.personal.sendTransaction(tx_fee,passphrase, function(err, feeBHash) {
			    if (!err){
				var tx_fee_b={
					from:"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56",
					to:wallet_commission,
					value:web3.toWei(total_fee,"ether")
				}
				console.log("Fee hash: "+feeBHash);
				web3.personal.sendTransaction(tx_fee_b,"14b2442e95515e0b8ebf4c5cfc8cb36c8", function(err, feeBHash) {
				    if (!err){
						console.log("Return fee hash: "+feeBHash);
						res.send(JSON.stringify({
							answer:{
								txhash:transactionHash,
								fee_blockchain:total_fee,
								fee:1,
								fee_blockchain_hash:feeBHash
							},
							type:'success',
							msg:''
						}));
						res.end();
				    }else {console.log(err);res.end();}
				  });
			    }else {console.log(err);res.end();}
			  });
	  	  }else {console.log(err);res.end();}
		  
		});
	}else{
		console.log('Not enough balance');		
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
});


app.get("/listAccounts/",function(req,res){
//console.log(web3.eth.accounts);// in web3.eth.accounts
	//for(i=0;i<90;i++){
	for(i in web3.eth.accounts){
		var account = web3.eth.accounts[i];//"\tbalance:" + omisegoContract.balanceOf(account) + " UXC" + " and "+
		res.write("Account["+i+"]:\t"+ account + "\tbalance:"+ web3.fromWei(web3.eth.getBalance(account),'ether')+" FOX \tand "+toDec(unicashContract.balanceOf(account))+" UXC" + "\n");
		//console.log(unicashContract.balanceOf(account));		
		//if(i>5) return;
	}
	console.log("Get listAccounts");
	//var acc=['0x1fa7baba2df63499e05c7ab00b43df0aa8709f3f','0xd3728023da5cfe4a7cbf54d672ace80f427d91b5','0x362fef7aa46778647c143cada6a3d186e2266a03','0xcc009d311365afa9b3b4c2a2ce5c22e6d9248ff4','0x672c0f498e59426f67b8cd932a63c37c706f8fa0','0x035333e87cc1925329e48487bbd0d330737eda94'];
	//for(ii in acc){
	//	var account = acc[ii];
		//console.log("Account["+ii+"]: "+ account+ " "+web3.eth.getBalance(account));
		//res.write("Account["+ii+"]: "+ account + "\tbalance:"+ web3.fromWei(web3.eth.getBalance(account),'ether')+" FOX"+"\n");
	//}
	//web3.eth.accounts.forEach(function(e,i){
	//	res.write("eth.accounts["+i+"]: "+ web3.eth.accounts[i] + "\tbalance:" + omisegoContract.balanceOf(web3.eth.accounts[i]) + " UXC" + " and "+ web3.eth.getBalance(web3.eth.accounts[i])+" FOX"+"\n")
	//});
	res.end();
});

//request for cron
//update to run it together with uxc
app.post("/getBalanceAddresses/",function(req,res){
	var wallets={};
	var wallets_arr=req.body.wallets;
	console.log(wallets_arr);
	if(wallets_arr){
		for(i in wallets_arr){
			var account = wallets_arr[i];
			wallets[account]={
				'fox':{balance:web3.fromWei(web3.eth.getBalance(account),'ether')},
				'uxc':{balance:toDec(unicashContract.balanceOf(account))}
			};
		}
		res.send(JSON.stringify({
			answer:{
				wallets:wallets,
			},
			type:'success',
			msg:''
		}));
	}
	res.end();
});

//old version
/*app.post("/getBalanceAddresses/",function(req,res){
	var wallets={};
	for(i in web3.eth.accounts){
		var account = web3.eth.accounts[i];
		wallets[account]={balance:web3.fromWei(web3.eth.getBalance(account),'ether')};		
//res.write("Account["+i+"]: "+ account + "\tbalance:"+ web3.fromWei(web3.eth.getBalance(account),'ether')+" FOX"+"\n");
		//if(i>5) return;
	}
	res.send(JSON.stringify({
		answer:{
			wallets:wallets,
		},
		type:'success',
		msg:''
	}));
	res.end();
});
*/
//request for cron
//Edit getBalanceAddresses and delete this method
app.post("/getBalanceUxcAddresses/",function(req,res){
	var wallets={};
	for(i in web3.eth.accounts){
		var account = web3.eth.accounts[i];
		wallets[account]={balance:toDec(unicashContract.balanceOf(account))};
	}
	res.send(JSON.stringify({
		answer:{
			wallets:wallets,
		},
		type:'success',
		msg:''
	}));
	res.end();
});


app.post("/sendUxcTransaction/", function(req, res) {
	
	var from=req.body.from;
	var to=req.body.to;
	var amount=req.body.amount;
	var passphrase=req.body.pass;
	var CurrentGasPrice=web3.eth.gasPrice;

	let transfer_data = unicashContract.transfer.getData(to,amount,{from: from});
	var wallet_to_contract="0x1993449779fb777030af9be12287747547e3a66a";
	var wallet_commission="0xe759a1967b97d15c4a54654fdc5c2236108d9cd6";
	var fee_commission=1;//UXC

	var tx={
	  	from: from,			
		to: wallet_to_contract,
		value:0,
		data:transfer_data	
	}
	console.log(tx);

	let gas_used = web3.eth.estimateGas(tx);
	//console.log("Gas used: "+gas_used);
	console.log("Gas Price: "+CurrentGasPrice);
	var fee=web3.fromWei(gas_used*CurrentGasPrice,"ether");
	//console.log("Fee: "+fee);
	//web3.personal.unlockAccount(from,passphrase,100);
	
	let transfer_data_fee = unicashContract.transfer.getData(wallet_commission,fromDec(fee_commission),{from: from});		

	var tx_fee={
		to: wallet_to_contract,
		from: from,
		value: 0,
		data:transfer_data_fee
	}
	var gas_used_for_fee = web3.eth.estimateGas(tx_fee);

	var fee_2=web3.fromWei(gas_used_for_fee*CurrentGasPrice,"ether");
	var total_fee=Number(fee)+Number(fee_2);
	if(unicashContract.balanceOf(from)>fromDec(amount+fee_commission)){
		if(web3.eth.getBalance(from)>web3.toWei(total_fee,"ether")){
			//passphrase="42258b65dddbda4309995ac07a32fbe8";
			web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) { //first transaction
			if (!err){
				console.log("Send main transaction");
				console.log("Gas used (fee)= "+gas_used);
				console.log("wallet from "+from+" Balance "+web3.fromWei(web3.eth.getBalance(from),"ether"));
				console.log("wallet to "+to+" Balance "+web3.fromWei(web3.eth.getBalance(to),"ether"));
				console.log("wallet coinbase "+"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"+" Balance "+web3.fromWei(web3.eth.getBalance("0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56"),"ether"));
				console.log("wallet for commision "+wallet_commission+" Balance "+web3.fromWei(web3.eth.getBalance(wallet_commission),"ether"));
				  
				console.log("fee:"+fee+" fee_2 "+fee_2);
				console.log("fee sum= "+web3.toWei(Number(fee)+Number(fee_2),"ether"));
		
				  web3.personal.sendTransaction(tx_fee,passphrase, function(err, feeBHash) { //second transaction commission
				    if (!err){
					var tx_fee_b={
						from:"0x3e6e5a04036a9a11d0b5e2fa6e96c9b32462fe56",
						to:wallet_commission,
						value:web3.toWei(total_fee,"ether")
					}
					web3.personal.sendTransaction(tx_fee_b,"14b2442e95515e0b8ebf4c5cfc8cb36c8", function(err, feeBHash) { // third transaction send sum fee to wallet fee
					    if (!err){
						console.log(transactionHash);
					  	res.send(JSON.stringify({
							answer:{
								txhash:transactionHash,
								fee_blockchain:total_fee,
								fee:fee_commission,
								fee_blockchain_hash:feeBHash
							},
							type:'success',
							msg:''
						}));
						res.end();
					    }else {console.log(err);res.end();}
					  });
				    }else {console.log(err);res.end();}
				  });
		  	  }else {console.log(err);res.end();}
			  
			});
		}else{
			res.send(JSON.stringify({
				answer:null,
				type:'error',
				msg:'Not enough balance'
			}));
			res.end();
		}
	}else{
		res.send(JSON.stringify({
			answer:null,
			type:'error',
			msg:'Not enough balance'
		}));
		res.end();
	}
});

function fromDec(number){
	if(number>=0){
		return number*100000000;	
	}else return false;

}
function toDec(number){
	if(number>=0){
		return number/100000000;	
	}else return false;

}

async function get_transfer_data(from,to,amount,passphrase) {
	if(passphrase){
		var CurrentGasPrice=web3.eth.gasPrice;
		web3.personal.unlockAccount(from,passphrase,100);

		let transfer_data = unicashContract.transfer.getData(to,amount,{from: from});
		let gas_limit = web3.eth.estimateGas({
			from: from,			
			to: "0x9891c0d7232637c177f3a6ab51932b0190e1fc6a",
			data:transfer_data
		});
		console.log("Gas for transaction "+gas_limit);
		var GasPrice=CurrentGasPrice;//Math.floor(web3.toWei(1)/gas_limit);
		var block = web3.eth.getBlock("latest");
		console.log("current gas Limit = " + block.gasLimit);
		console.log("current gas Price = " + CurrentGasPrice);
		
console.log("Max fee = " + web3.fromWei((block.gasLimit*CurrentGasPrice), 'ether')+' FOX');
		console.log("New Max fee = " + web3.fromWei((block.gasLimit*GasPrice), 'ether')+' FOX');
		console.log("New fee 1 = " + web3.fromWei((gas_limit*GasPrice), 'ether')+' FOX');
		console.log("New fee 2 = " + web3.fromWei((new_gas_limit*GasPrice), 'ether')+' FOX');
		console.log("balance before = " + web3.fromWei(web3.eth.getBalance(web3.eth.accounts[0]), 'ether')+' FOX');
		return omisegoContract.transfer(to,amount,{from: from});
	}
	
}

app.get("/sendToken/:from/:to/:amount",function(req,res){
	var from = req.params.from,
	to = req.params.to,
	amount = req.params.amount||0;
	res.set('Content-Type','application/json');

	//web3.eth.getGasPrice(function(e,r){console.log("Gas Price = "+r)});
	//web3.eth.getGasLimit(function(e,r){console.log("Gas Limit = "+r)});	

	get_transfer_data(from,to,amount).then((txhash) => {
		//console.log(txhash);
	console.log("balance after = " + web3.fromWei(web3.eth.getBalance(web3.eth.accounts[0]), 'ether')+' FOX');
		res.send(JSON.stringify({
			answer:{
				txhash:txhash
			},
			type:'success',
			msg:''
		}));
	});
		
});

/*function DBConnectInit() {
  return new Promise(connect => {
	DB = mysql.createConnection({
		host: "localhost",
		user: "root",
		password: "dsk8jhf6dsf3hj7d3os",
		database: "blockchain"
	});

    	DB.connect((err) => {
		if (err) throw err;
		isConnect = true;
		connect('Connected');
    	});
  });
}*/




/*function GetPassphrase(from) {
  return new Promise(callback => {
	DB.query("SELECT passphrase FROM `cr__wallets` WHERE `wallet`='"+from+"' LIMIT 1", function (err, result, fields) {
	    	if (err) throw err;

		//for(i in result){
		//	var row = result[i]; 
		//}		
		if(result.length) callback(result[0].passphrase); else result(false);
	});
  });
}*/



http.createServer(app).listen(7000);
