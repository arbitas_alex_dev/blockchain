# Устрановка и создание блокчейна
## PART 1. Установка необходимого ПО

**Все инструкции составлены для Ubuntu 16.04 LTS на 31.08.2018**

Установка nodejs, npm, curl, git ...

```
apt-get install nodejs npm nodejs-legacy build-essential libssl-dev curl git
```

Установка NVM

```
curl https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

После установки NVM необходимо перезагрузить терминал

Далее устанавливаем

```
nvm ls-remote
nvm install 8.11.3
nvm install 9.4.0
```

Создаем node server, который позволит работать с блокчейном
```
cd /var/
sudo mkdir node_server
cd /var/node_server
```

Копируем в созданную папку файл из проекта ```nodejs/index.js```. Инициализируем node проект и устанавливаем необходимые библиотеки.

```
npm install
sudo npm install web3@^0.20.6
sudo npm install express
sudo npm install keythereum
```

Для проверки можно запустить команду ```node index.js```. В консоли не должно быть ошибок. Прерываем в выполнение через ```CTRL+C```.

**Установка программ мониторинга блокчейна.**

Подробнее:

> https://github.com/cubedro/eth-net-intelligence-api

> https://github.com/cubedro/eth-netstats

Установка intelligence api:

```
cd /var/
sudo git clone https://github.com/cubedro/eth-net-intelligence-api
bash <(curl https://raw.githubusercontent.com/cubedro/eth-net-intelligence-api/master/bin/build.sh) 
# в диалоговом окне необходимо выбрать вариант 2. geth
cd eth-net-intelligence-api
npm install
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Установка netstats:

```
cd /var/
git clone https://github.com/cubedro/eth-netstats
cd eth-netstats
npm install
sudo npm install -g grunt-cli
grunt all
npm start
```

## Запуск программ и установка блокчейна

Необходимо зайти в папку intelligence api ```cd /var/eth-net-intelligence-api```, найти файл processes-ec2.json и переименовать в processes.json. 

Далее открываем файл processes.json и заменяем его содержимое на:

processes.json:
```json
[
  {
    "name"              : "<PROCESS_NAME>",
    "cwd"               : "/var/eth-net-intelligence-api",
    "script"            : "app.js",
    "log_date_format"   : "YYYY-MM-DD HH:mm Z",
    "log_file"          : "/var/eth-net-intelligence-api/log/node-app-log.log",
    "out_file"          : "/var/eth-net-intelligence-api/log/node-app-out.log",
    "error_file"        : "/var/eth-net-intelligence-api/log/node-app-err.log",
    "merge_logs"        : false,
    "watch"             : false,
    "max_restarts"      : 10,
    "exec_interpreter"  : "node",
    "exec_mode"         : "fork_mode",
    "env":
    {
      "NODE_ENV"        : "production",
      "RPC_HOST"        : "localhost",
      "RPC_PORT"        : "8545",
      "LISTENING_PORT"  : "30303",
      "INSTANCE_NAME"   : "<PROCESS_NAME>",
      "CONTACT_DETAILS" : "",
      "WS_SERVER"       : "http://localhost:3000",
      "WS_SECRET"       : "<WS_SECRET>",
      "VERBOSITY"       : 2
    }
  }
] 
```

где:

**<PROCESS_NAME>** - имя процесса (приложения); 

**<WS_SECRET>** - секретный ключ для netstats (в netstats задается при запуске, см. ниже).

Пример заполненного файла:

```json
[
  {
    "name"              : "int-api",
    "cwd"               : "/var/eth-net-intelligence-api",
    "script"            : "app.js",
    "log_date_format"   : "YYYY-MM-DD HH:mm Z",
    "log_file"          : "/var/eth-net-intelligence-api/log/node-app-log.log",
    "out_file"          : "/var/eth-net-intelligence-api/log/node-app-out.log",
    "error_file"        : "/var/eth-net-intelligence-api/log/node-app-err.log",
    "merge_logs"        : false,
    "watch"             : false,
    "max_restarts"      : 10,
    "exec_interpreter"  : "node",
    "exec_mode"         : "fork_mode",
    "env":
    {
      "NODE_ENV"        : "production",
      "RPC_HOST"        : "localhost",
      "RPC_PORT"        : "8545",
      "LISTENING_PORT"  : "30303",
      "INSTANCE_NAME"   : "int-api",
      "CONTACT_DETAILS" : "",
      "WS_SERVER"       : "http://localhost:3000",
      "WS_SECRET"       : "test_blokchain_123456",
      "VERBOSITY"       : 2
    }
  }
] 
```

Далее запускаем nodejs приложения через pm2 (он был установлен через bash скрипт в 1 пункте):

```
pm2 start /var/node_server/index.js -o /var/node_server/log.log
WS_SECRET=<WS_SECRET> pm2 start /var/eth-netstats/app.js -o /var/eth-netstats/log_work.log
pm2 start /var/eth-net-intelligence-api/processes.json
```
где:

**<WS_SECRET>** - тот секретный ключ, который был указан в processes.json, для примера выше, запуск будет следующим

```
pm2 start /var/node_server/index.js -o /var/node_server/log.log
WS_SECRET=test_blokchain_123456 pm2 start /var/eth-netstats/app.js -o /var/eth-netstats/log_work.log
pm2 start /var/eth-net-intelligence-api/processes.json
```

**Создание блокчейна.**

Необходимо создать сервис

```
cd /root
```

Создаем в ```/root``` файл с именем ```geth.service``` со следующим содержимым

```
### BEGIN INIT INFO
[Unit]
Description=Geth

[Service]
Type=simple
User=root
Restart=always
WorkingDirectory=/root
ExecStart=/usr/bin/geth --config config.toml #2>>/var/test_blockchain/01.log - не работает на Ubuntu 16.04

[Install]
WantedBy=default.target
### END INIT INFO
```

Создаем (генерируем) ```config.toml``` в ```/root```.

```
geth --rpc --rpccorsdomain "*" --datadir "<BLOCKCHAIN_DIR>" --rpcport "8545" --port "30303" --rpcapi="db,eth,net,web3,personal,web3" --verbosity 6 --ipcdisable --networkid <CHAIN_ID> dumpconfig > config.toml
```

где:

**<BLOCKCHAIN_DIR>** - директория, где будет храниться все связанное с блокчейном; 

**<CHAIN_ID>** - обеспечивает способ совершения транзакций в Ethereum без использования ETC (Ethereum Classic) или тестовой сети Morden. EIP 155 предусматривает следующие значения chainid для разных сетей: основная сеть Ethereum (1), основная сеть Morden / Expanse (2), Ropsten (3), Rinkeby (4), основная сеть Rootstock(30), тестовая сеть Rootstock (31), Kovan (42), основная сеть Ethereum Classic (61), тестовая сеть Ethereum Classic (62), приватные цепочки geth (1337 по умолчанию).

Мы будем использовать идентификатор, который не используется ни одной из существующих цепей, например ```20```, а в качестве директории ```/var/test_blockchain/```. Тогда наша команда будет выглядеть следующим образом:

```
geth --rpc --rpccorsdomain "*" --datadir "/var/test_blockchain/" --rpcport "8545" --port "30303" --rpcapi="db,eth,net,web3,personal,web3" --verbosity 6 --ipcdisable --networkid 20 dumpconfig > config.toml
```

Далее создаем и запускаем демон:

```
cp ./geth.service /etc/systemd/system/geth.service
systemctl daemon-reload
systemctl enable geth.service
systemctl start geth
```

Заходим в консоль блокчейна

```
geth attach <BLOCKCHAIN_DIR>/geth.ipc
```

Создаем новый кошелек

```
personal.newAccount("<WALLET_PASSWORD>")
```

Выходим из консоли блокчейна используя команду ```exit```

где:

**<WALLET_PASSWORD>** - пароль кошелька

Результатом выполнения будет строка - адрес кошелька (далее ```<WALLET_NAME>```). Запишите адрес и пароль кошелька.

Далее создаем файл в папке блокчейна (в примере ```/var/test_blockchain/```) файл ```genesis.json``` со следующим содержимым:

```json
{
    "config": {
        "chainId": <CHAIN_ID>,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
    "difficulty": "1000000",
    "gasLimit": "100000000",
    "alloc": {
        "<WALLET_NAME>": 
         { "balance": "1000000000000000000000000000" }
    }
}
```

где:

**<CHAIN_ID>** - идентификатор, который мы указали при генерации файла ```config.toml```;

**<WALLET_NAME>** - адрес кошелька, который был создан на предыдущем шаге. Баланс кошелька указывается с 18 нулями от нужного числа, т.е. нужно желаемый баланс кошелька умножить на 10^18

Далее необходимо перезапустить блокчейн, но уже с файлом ```genesis.json```

Останавливаем geth

```
systemctl stop geth
```

Удаляем папку ```geth``` из папки с блокчейном (в примере ```/var/test_blockchain/geth```).

Выполняем команду

```
geth --datadir "<BLOCKCHAIN_DIR>" init "<BLOCKCHAIN_DIR>/genesis.json"
```

где:

**<BLOCKCHAIN_DIR>** - директория блокчейна;

В случае из примера команда вылядит так:

```
geth --datadir "/var/test_blockchain/" init "/var/test_blockchain/genesis.json"
```

После чего опять запускаем geth

```
systemctl start geth
```

Заходим в консоль geth ```geth attach <BLOCKCHAIN_DIR>/geth.ipc```. Необходимо разблокировать кошелек.
```
personal.unlockAccount("<WALLET_NAME>","<WALLET_PASSWORD>",2000)
```

Также (в консоли geth) запускаем майнер
```
miner.start()
```

После чего выходим из консоли.

Устанавливаем truffle (Подробнее: https://github.com/trufflesuite/truffle). Он необходим для создания токена и компиляции контракта.
```
npm install -g truffle
```

Переносим папку ```truffle/``` на сервер в ```/root``` и заходим в нее ```cd /root/truffle```.

В файле ```truffle/truffle.js``` можно поправить цену газа.

В файле ```truffle/migrations/2_deploy_contracts.js``` можно название валюты (<CURRENCY_NAME>), короткий символ валюты (<CURRENCY_SHORT>), и максимальное кол-во валюты (<CURRENCY_MAX>).

```
//...
.then(() => {
            return deployer.deploy(
                AdvancedToken, <CURRENCY_MAX>,"<CURRENCY_NAME>", "<CURRENCY_SHORT>")
	});
//...
```

Из папки ```/root/truffle``` выполняем команды:

```
truffle compile
truffle migrate
```

В результате выполнения ```truffle migrate``` мы получим строку которая содержит строчки:

```
Replacing MyAdvancedToken...
  ... 0x4b3159ccd5789a7baa4c7ebea6af637f35128035b54e00780bda0f39f164edbb
  AdvancedToken: 0x47bc949dd2f2c338a6a15ed14fe3c998b76fcac1
```

Адрес токена ```AdvancedToken: 0x47bc949dd2f2c338a6a15ed14fe3c998b76fcac1``` (далее <TOKEN_ADDRESS>) необходимо запомнить.

Далее нужно получить контракт для index.js. Установим solc 0.4.24. Можно скачать отсюда (https://github.com/ethereum/solidity/releases/tag/v0.4.24)

```
sudo apt-get install solc
```

После чего выполним команду

```
solc /root/truffle/contracts/AdvancedToken.sol --abi
```

В результате мы должны получить следующий ответ:

```
======= /root/truffle/contracts/AdvancedToken.sol:AdvancedToken =======
Contract JSON ABI
[{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"minBalanceForAccounts","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"minimumBalanceInFinney","type":"uint256"}],"name":"setMinBalance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}]

======= /root/truffle/contracts/AdvancedToken.sol:TokenERC20 =======
Contract JSON ABI
[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}]

======= /root/truffle/contracts/AdvancedToken.sol:owned =======
Contract JSON ABI
[{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]
```

Нас интересует json от ```/root/truffle/contracts/AdvancedToken.sol:AdvancedToken```, в данном примере следуюшая срока (далее <JSON_CONTRACT>):

```
[{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"minBalanceForAccounts","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"minimumBalanceInFinney","type":"uint256"}],"name":"setMinBalance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}]
```

Возвращаемся к файлу index.js (```/var/node_server/index.js```)

В нем необходимо найти следующие строчки (имя переменных может отличаться)

```
var uxcabi = [{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"minBalanceForAccounts","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner2","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner1","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"secondOwner","type":"address"}],"name":"setSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"minimumBalanceInFinney","type":"uint256"}],"name":"setMinBalance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}];

var unicashContract = web3.eth.contract(uxcabi).at('0xc256c37b1d3d9dc25ce0b75c119265a21f54d407');
```

В них необходимо заменить данные на наши, по следующему шаблону

```
var uxcabi = <JSON_CONTRACT>;

var unicashContract = web3.eth.contract(uxcabi).at('<TOKEN_ADDRESS>');
```

После необходимо перезапустить скрипты в pm2:

```
pm2 update
```

## Завершение и Проверка

По адресу http://<SERVER_IP>:3000/ (где **<SERVER_IP>** - ip адрес сервера) должна быть доступна админка (http://joxi.ru/n2YBOa5coR79PA).

По адресу http://<SERVER_IP>:7000/listAccounts должен быть доступен список аккаунтов в блокчейне (http://joxi.ru/DrllpoVHvJG1vr).

Протестируем перевод средств

Заходим в консоль geth ```geth attach <BLOCKCHAIN_DIR>/geth.ipc```. Создаем новый кошелек

```
personal.newAccount("<NEW_WALLET_PASSWORD>")
```

Сохраняем новый адрес кошелька (далее <NEW_WALLET_NAME>). 

В файле ```/var/node_server/index.js``` добавляем код (например после метода sendTransaction)

```js
app.get('/sendTestTransaction/', function (req, res){
    
    var from = "<WALLET_NAME>";
    var wallet_to_contract = "<TOKEN_ADDRESS>";
    
    var passphrase = '<WALLET_PASSWORD>';
    
    let transfer_data = unicashContract.transfer.getData('<NEW_WALLET_NAME>',1000000000,{from: from});
    
    var tx = {
        from: from,			
        to: wallet_to_contract,
        value:0,
        data:transfer_data	
    }
    
    web3.personal.sendTransaction(tx,passphrase, function(err, transactionHash) {
        if (!err){
            res.send(JSON.stringify({answer:{txhash:transactionHash}, type:'success', msg:''}));
            res.end();
        } else {
            console.log(err);res.end();
        }
    });
    
});
```

Выполняем тестовую транзакцию по адресу http://<SERVER_IP>:7000/sendTestTransaction

Результат проверяем по адресу http://<SERVER_IP>:7000/listAccounts